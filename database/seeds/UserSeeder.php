<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Models\Role;
use App\Models\Informasi;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Create Pengaturan role
        $pengaturanKonten = new Role();
        $pengaturanKonten->name = 'pengaturan konten';
        $pengaturanKonten->display_name = 'Menu Pengaturan Konten';
        $pengaturanKonten->description = 'Menu Pengaturan Master Konten';
        $pengaturanKonten->save();

        //Create User role
        $pengaturanUserRole = new Role();
        $pengaturanUserRole->name = 'pengaturan user';
        $pengaturanUserRole->display_name = 'Menu Pengaturan User';
        $pengaturanUserRole->description = 'Menu Pengaturan Master User';
        $pengaturanUserRole->save();

        $administrator = User::create([
            'name' => 'master',
            'email' => 'master@app.com',
            'password' => Hash::make('passwordweb'), // optional
        ]);
        $administrator->attachRole($pengaturanKonten);
        $administrator->attachRole($pengaturanUserRole);

        //Create Informasi
        $informasi = new Informasi();
        $informasi->nama = 'INTEKDIGI';
        $informasi->alamat = '';
        $informasi->email = '';
        $informasi->alamat = '';
        $informasi->facebook = '';
        $informasi->instagram = '';
        $informasi->youtube = '';
        $informasi->file = 'logo';
        $informasi->save();
    }
}
